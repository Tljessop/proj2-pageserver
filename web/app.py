from flask import Flask, render_template, abort
import fnmatch        # For pattern matching in handling forbidden requests


app = Flask(__name__)
@app.route("/<top_path>/<path>")
@app.route("/<path>")
def serve(path,top_path=""):
    app.logger.info(path)
    if fnmatch.fnmatch(path,"*~*") or fnmatch.fnmatch(path,"*..*") or fnmatch.fnmatch(path,"*//*"):
        forbidden(abort(403))
    try:
        with open(path,"r") as page:
            html_page = page.read()
        return html_page
    except:
        not_found(abort(404))

@app.errorhandler(404)
def not_found(e):
    with open("pages/404.html","r") as page:
            error_page404 = page.read()
    return error_page404, 404

@app.errorhandler(403)
def forbidden(e):
    with open("pages/403.html","r") as page:
            error_page403 = page.read()
    return error_page403, 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
